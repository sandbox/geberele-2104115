<?php

/**
 * Implements template_preprocess_hook().
 *
 * @param array $vars
 */
function template_preprocess_spam_finder(&$vars) {
  $query = "SELECT * FROM node";
  $limit1 = " LIMIT 0, 1";
  $q = db_query($query . $limit1);
  $r = db_fetch_array($q);
  $header = array();
  $i = 0;
  foreach ($r as $key => $value) {
    if ($i == 0)
      $header[$i]['sort'] ='desc';
    $header[$i]['data'] = $key;
    $header[$i]['field'] = $key;
    $i ++;
  }

  $element = 0;
  $limit = 5;
  $count_query = "SELECT COUNT(*) FROM (" . $query . ") AS count_query";
  $q = pager_query( $query . tablesort_sql($header), $limit, $element, $count_query);
  while ($r = db_fetch_array($q)) {
    $rows[] = $r;
  }

  $vars['table'] = theme('table', $header, $rows);
  $vars['pager'] = theme('pager',NULL, $limit, $element);
}